/** @type {import('next').NextConfig} */
const config = {
  swcMinify: true,
  reactStrictMode: false,
  experimental: {
    appDir: false
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack']
    });
    return config;
  }
};

// Remove this if you're not using Fullcalendar features
const withTM = require('next-transpile-modules')([
  '@fullcalendar/common',
  '@fullcalendar/react',
  '@fullcalendar/daygrid',
  '@fullcalendar/list',
  '@fullcalendar/timegrid',
  '@fullcalendar/timeline'
]);

module.exports = {
  basePath: '/auth/jwt/login',
}

module.exports = withTM(config);
