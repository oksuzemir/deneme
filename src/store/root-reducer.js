import { combineReducers } from "@reduxjs/toolkit";
import tenantReducer from "./tenantReducer";
import userReducer from "./userReducer";

export const rootReducer = combineReducers({
  tenant: tenantReducer,
  user: userReducer,
});
