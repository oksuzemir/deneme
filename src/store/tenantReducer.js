const INITIAL_STATE = {
  tenant: "Doctor Bircan",
};

const generalReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "CHANGE_TENANT":
      return { ...state, tenant: action.payload };
    default:
      return state;
  }
};

export default generalReducer;
