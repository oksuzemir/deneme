import { SvgIcon } from "@mui/material";
import HomeSmileIcon from "../../icons/untitled-ui/duocolor/home-smile";
import LayoutAlt02Icon from "../../icons/untitled-ui/duocolor/layout-alt-02";
import Users03Icon from "../../icons/untitled-ui/duocolor/users-03";
import { tokens } from "../../locales/tokens";
import { paths } from "../../paths";

let sectionsArray = null;

if (typeof window !== "undefined") {
  if (localStorage.getItem("activeProject") === "Doctor Bircan") {
    sectionsArray = [
      {
        items: [
          {
            title: t(tokens.nav.overview),
            path: paths.dashboard.index,
            icon: (
              <SvgIcon fontSize="small">
                <HomeSmileIcon />
              </SvgIcon>
            ),
          },
        ],
      },
      {
        subheader: t(tokens.nav.pages),
        items: [
          {
            title: t(tokens.nav.blog),
            path: paths.dashboard.blog.index,
            icon: (
              <SvgIcon fontSize="small">
                <LayoutAlt02Icon />
              </SvgIcon>
            ),
            items: [
              {
                title: t(tokens.nav.postList),
                path: paths.dashboard.blog.index,
              },

              {
                title: t(tokens.nav.postCreate),
                path: paths.dashboard.blog.postCreate,
              },
            ],
          },
        ],
      },
    ];
  } else if (localStorage.getItem("activeProject") === "COSyLife App") {
    sectionsArray = [
      {
        items: [
          {
            title: t(tokens.nav.overview),
            path: paths.dashboard.index,
            icon: (
              <SvgIcon fontSize="small">
                <HomeSmileIcon />
              </SvgIcon>
            ),
          },
        ],
      },
      {
        subheader: t(tokens.nav.pages),
        items: [
          {
            title: t(tokens.nav.customers),
            path: paths.dashboard.customers.index,
            icon: (
              <SvgIcon fontSize="small">
                <Users03Icon />
              </SvgIcon>
            ),
            items: [
              {
                title: t(tokens.nav.list),
                path: paths.dashboard.customers.index,
              },
            ],
          },
          {
            title: t(tokens.nav.blog),
            path: paths.dashboard.blog.index,
            icon: (
              <SvgIcon fontSize="small">
                <LayoutAlt02Icon />
              </SvgIcon>
            ),
            items: [
              {
                title: t(tokens.nav.postList),
                path: paths.dashboard.blog.index,
              },

              {
                title: t(tokens.nav.postCreate),
                path: paths.dashboard.blog.postCreate,
              },
            ],
          },
        ],
      },
    ];
  }
}

export const getSections = sectionsArray;
