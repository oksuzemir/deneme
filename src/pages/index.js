import Head from "next/head";
import { usePageView } from "../hooks/use-page-view";
import ModernLayout from "../layouts/auth/modern-layout";
import Auth from "./auth/amplify/login";

const Page = () => {
  usePageView();

  return (
    <>
      <Head>
        <title>COS - Admin</title>
      </Head>
      <main>
        <Auth />
      </main>
    </>
  );
};

Page.getLayout = (page) => <ModernLayout>{page}</ModernLayout>;

export default Page;
